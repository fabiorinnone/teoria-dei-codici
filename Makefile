# Makefile per documento in LaTeX

default: all

all:
	pdflatex appunti_teoria_codici.tex
	#bibtex appunti_teoria_codici
	pdflatex appunti_teoria_codici.tex
	#latex appunti_teoria_codici.tex
	#dvips -t a4 appunti_teoria_codici.dvi -o
	#ps2pdf appunti_teoria_codici.ps

.tex:
	pdflatex $@
	#dvips -t a4 $?.dvi -o
	#ps2pdf $@.ps

clean:
	rm -f *.aux
	rm -f *.log
	rm -f *.toc
	rm -f *.loa
	rm -f *.lot
	rm -f *.lof
	rm -f *.idx
	rm -f *.bbl
	rm -f *.blg
	rm -f *.idx
	rm -f *.ilg
	rm -f *.ind
	rm -f *.ps
	rm -f *.out
	rm -f *~

erase:
	rm -f *.aux
	rm -f *.log
	rm -f *.toc
	rm -f *.loa
	rm -f *.lot
	rm -f *.lof
	rm -f *.idx
	rm -f *.bbl
	rm -f *.blg
	rm -f *.idx
	rm -f *.ilg
	rm -f *.ind
	rm -f *.ps
	rm -f *.out
	rm -f *~
	#rm -f *.dvi*
	#rm -f *.ps*
	rm -f *.pdf*

help:
	@echo "make [all]: genera i file .dvi e .pdf di tutti i file .tex"
	@echo "make clean: elimina i file non necessari"
	@echo "make erase: elimina i file non necessari e i file .dvi, .ps e .pdf"
	@echo "Leggere anche README"
