%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%% Capitolo 4 %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Codici cicicli}

\begin{defin}[Permutazione ciclica]
Chiamiamo \textit{permutazione ciclica} l'applicazione $\pi:\mathbb{K}^n
\rightarrow\mathbb{K}^n$ così definita.
\begin{displaymath}
\pi(a_0a_1\dots a_n)=a_na_0\dots a_{n-1}.
\end{displaymath}
\end{defin}

\begin{ese}
$1011\xrightarrow{\pi} 1101$.
\end{ese}

\begin{defin}
Un codice $C\in\mathbb{K}^n$ si dirà ciclico se è \textit{invariante}
rispetto a permutazioni cicliche, ovvero
\begin{displaymath}
\forall c\in C\Rightarrow\pi(c)\in C.
\end{displaymath}
\end{defin}

\begin{ese}
$C=\{\ve{0}\}$.
\end{ese}

\begin{ese}
$C=\{\ve{0},\ve{1}\}$.
\end{ese}

\begin{ese}
$C=\{101,110,011,000\}=\langle 101,110\rangle$. ($C$ è codice lineare).

$\pi(101)=110$.

$\pi(110)=011$.

$\pi(011)=101$.
\end{ese}

\begin{ese}
$C=\{01,10\}$ è ciclico ma non lineare.
\end{ese}

$\pi:\mathbb{K}^n\rightarrow\mathbb{K}^n$ è un'applicazione lineare ovvero
\begin{displaymath}
\pi(\ve{x}+\ve{y})=\pi(\ve{x})+\pi(\ve{y})\hspace{2mm}\forall\ve{x},\ve{y}
\in\mathbb{K}^n\hspace{5mm}\text{(additività)}
\end{displaymath}
ed anche
\begin{displaymath}
\pi(\alpha+\ve{x})=\alpha+\pi(\ve{x})\hspace{2mm}\forall\alpha\in\mathbb{K},\ve{x}
\in\mathbb{K}^n\hspace{5mm}\text{(omogeneità).}
\end{displaymath}

(Nel caso dei campi finiti $\mathbb{K}=\{0,1\}$ è sufficiente che sia verificata
la sola proprietà di additività).

\begin{teo}
$C=\langle B\rangle$ è ciclico se e solo se $B$ è invariante rispetto a $\pi$.
\end{teo}

(Il teorema segue direttamente dalla definizione di linearità di $\pi$).

In particolare si può scegliere come insieme di generatori $B$ una base di $C$.

\vspace{3mm}

Sia $\ve{x}\in\mathbb{K}^n$. L'insieme costituito da $\ve{x},\pi(\ve{x}),
\pi(\pi(\ve{x}))=\pi^2(\ve{x}),\dots,\pi^{n-1}(\ve{x})$ è un insieme di generatori
per un codice ciclico.

\vspace{3mm}

\textbf{Polinomi a coefficienti su }$\mathbb{K}$. Un polinomio di grado $n$ è
un'espressione formale di questo tipo
\begin{displaymath}
a_0+a_1x+\dots +a_nx^n\text{ con }a_0,a_1,\dots,a_n\in\mathbb{K}.
\end{displaymath}

Se $a_n\neq 0$ diremo che $a_0+a_1x+\dots +a_nx^n$ ha grado $n$. L'insieme di tutti
i polinomi si indica con $\mathbb{K}[x]$ (il sottoinsieme dei polinomi di grado
$\leq n$ si indica con $\mathbb{K}[x]_n$).

Con l'operazione di somma tra polinomi e di prodotto tra uno scalare ed un 
polinomio gli insiemi $\mathbb{K}[x]$ e $\mathbb{K}[x]_n$ sono spazi vettoriali.

Inoltre
\begin{displaymath}
\mathbb{K}[x]_1\subseteq \mathbb{K}[x]_2\subseteq\dots\mathbb{K}[x]_n\subseteq\mathbb{K}[x].
\end{displaymath}

\textbf{Corrispondenza tra polinomi e parole}. Consideriamo una parola, avremo
\begin{displaymath}
a_0a_1\dots a_n\mapsto a_0+a_1x+\dots +a_nx^n.
\end{displaymath}

\begin{ese}
$x^3+x^5+1+x=\underbrace{1+x+x^3+x^5}_{=1+1\cdot x+0\cdot x^2+1\cdot x^3+0\cdot x^4+1\cdot x^5}
\mapsto 110101.$
\end{ese}

\begin{ese}
$\mathbb{K}[x]_2$ è lo spazio vettoriale (insieme) dei polinomi di grado $\leq 2$.

$\mathbb{K}[x]_2\ni 1+x^2,x+x^2,1+x,x,1$. L'insieme corrispondente delle parole è

\begin{displaymath}
\{101,011,110,010,100\}.
\end{displaymath}
\end{ese}

Moltiplichiamo un polinomio $g(x)$ per $x$. Avremo
\begin{displaymath}
g(x)=a_0+a_1x+\dots +a_nx^n\xrightarrow{x\cdot g(x)}a_0x+a_1x+\dots +a_nx^{n+1}.
\end{displaymath}

L'operazione equivale alla permutazione ciclica
\begin{displaymath}
a_0a_1\dots a_n\xrightarrow{\pi}a_na_0\dots a_{n-1}.
\end{displaymath}

\textbf{Divisione tra polinomi}. Dati $p(x),g(x)\in\mathbb{K}[x]$, con $p(x)$ dividendo e 
$g(x)$ divisore, esistono e sono unici due polinomi $q(x)\in\mathbb{K}[x]$ e $r(x)\in\mathbb{K}[x]$
con $\m{deg}(r(x))< \m{deg}(g(x))$ (grado di $r(x)$ minore del grado di $g(x)$) tali che
\begin{displaymath}
p(x)=q(x)\cdot g(x)+r(x). 
\end{displaymath}

\begin{ese}
$60\div 7$.
\begin{displaymath}
\longdiv{60}{7}
\end{displaymath}
\end{ese}

\begin{ese}
$p(x)=5x^4+x^3-x^2+2x+4,\hspace{2mm}g(x)=x^2-x+3$.
\begin{displaymath}
\polylongdiv{5x^4+x^3-x^2+2x+4}{x^2-x+3}
\end{displaymath}
$q(x)=5x^2+6x-10,\hspace{2mm}r(x)=-26x+34$.
\end{ese}

\begin{defin}
Diremo che due polinomi $f(x)$ e $g(x)$ sono \textit{congruenti} modulo $h(x)$ e lo indichiamo
con $f(x)\equiv g(x)\bmod h(x)$ se $f(x)$ e $g(x)$ hanno lo stesso resto nella divisione per
$h(x)$.
\end{defin}

Equivalentemente $f(x)$ e $g(x)$ sono congruenti modulo $h(x)$ se $f(x)-g(x)$ è un multiplo di
$h(x)$. Per dimostrare che un polinomio è multiplo di $h(x)$ è sufficiente dividere il polinomio
per $h(x)$ e verificare che il resto sia $0$).

\vspace{4mm}

Indichiamo con $\overline{f(x)}$ la classe di equivalenza modulo $h(x)$ avente $f(x)$ come
rappresentante.
\begin{displaymath}
\overline{f(x)}=\{g(x)\in\mathbb{K}[x]:g(x)\equiv f(x)\bmod h(x)\}
\end{displaymath}
ovvero l'insieme dei polinomi $g(x)$ tali che
\begin{displaymath}
g(x)-f(x)\text{ è multiplo di }h(x)
\end{displaymath}
ovvero
\begin{displaymath}
g(x)-f(x)=q(x)\cdot h(x)\text{ con }q(x)\in\mathbb{K}[x].
\end{displaymath}

Sia $g(x)\in\overline{f(x)}$ tale che $\m{deg}g(x)=\min\{\m{deg}a(x):a(x)\in\overline{f(x)},
a(x)\neq 0\}$ (per convenzione $a(x)=0\Rightarrow\m{deg}a(x)=-1)$. Tale $g(x)$ è unico, a meno
di multipli, cioè se $\m{deg}g(x)=\m{deg}f(x)=\min\{\m{deg}a(x):a(x)\in\overline{f(x)}\}$ allora
$g(x)=kf(x)$ con $k$ costante moltiplicativa non nulla.

Sia $f(x):\m{deg}f(x)=\m{deg}g(x)$ e applichiamo il teorema sulla divisione tra polinomi:
$f(x)=q(x)\cdot g(x)+r(x)$ con $\m{deg}r(x)<\m{deg}g(x)$ (oppure $r(x)=0$). Se $r(x)\neq 0$,
poiché $r(x)\in\overline{f(x)}=\overline{g(x)}$ ne seguirebbe l'assurdo che
\begin{displaymath}
\m{deg}r(x)<\min\{\m{deg}a(x):a(x)\in\overline{f(x)}\}.
\end{displaymath}

\begin{ese}
Troviamo una base del più piccolo codice ciclico contenente \linebreak
$\ve{v}=1101000$.
\begin{displaymath}
\begin{split}
g(x)& = 1+x+x^3 \\
xg(x)& = x+x^2+x^4 \\
x^2g(x)& = x^2+x^3+x^5 \\
x^3g(x)& = x^3+x^4+x^6 \\
x^4g(x)& = x^4+x^5+x^7
\end{split}
\end{displaymath}

Per descrivere tutte le parole di $\mathbb{K}^7$ abbiamo scelto di utilizzare tutti i
polinomi di grado $\leq 6$ ($\mathbb{K}[x]_6$). Poiché
\begin{displaymath}
x^7\equiv 1\bmod(1+x^7)
\end{displaymath}
riduciamo il polinomio $x^4g(x)$ modulo $(1+x^7)$. Otterremo
\begin{displaymath}
(x^4+x^5+x^7\equiv x^4+x^5+1)\bmod(1+x^7)
\end{displaymath}
Le parole del codice saranno
\begin{displaymath}
C=\langle \overline{g(x)},\overline{xg(x)},\overline{x^2g(x)},\overline{x^3g(x)},
\overline{x^4g(x)}\rangle
\end{displaymath}
dove con $\overline{x^ig(x)}$ si intende la classe di equivalenza modulo $(1+x^7)$. Una
matrice generatrice del codice (ciclico) $C$ si otterrà riducendo per righe la seguente
matrice
\begin{displaymath}
G=\left[
\begin{array}{ccccccc}
1 & 1 & 0 & 1 & 0 & 0 & 0 \\
0 & 1 & 1 & 0 & 1 & 0 & 0 \\
\vdots & \vdots & \vdots & \vdots & \vdots & \vdots & \vdots
\end{array}
\right].
\end{displaymath}
\end{ese}

Un codice ciclico di lunghezza $n$ si può interpretare come un insieme di classi
di equivalenza di polinomi modulo $(1+x^n)$.

\begin{oss}
Se $\ve{v}\in C$ (ciclico) e $v(x)\in\mathbb{K}[x]$ è il polinomio corrispondente allora
i polinomi $c(x)$ tali che $c(x)\equiv a(x)v(x)\bmod(1+x^n)$ sono \textit{parole} di $C$
e viceversa, se $c(x)$ è una \textit{parola} di $C$ allora $c(x)\equiv v(x)\bmod(1+x^n)$.
\end{oss}

\begin{ese}
Sia $\ve{v}=1101000$. Avremo
\begin{displaymath}
v(x)=1+x+x^3,\hspace{3mm}x^5v(x)=x^5(1+x+x^3)=x^5+x^6+x^8.
\end{displaymath}
Ma
\begin{displaymath}
x^5+x^6+x^8\equiv x^5+x^6+1\bmod(1+x^7)
\end{displaymath}
quindi
\begin{displaymath}
\overline{x^8}=\overline{x\cdot x^7}=\overline{x\cdot 1}.
\end{displaymath}
\end{ese}

\begin{defin}
Sia $C$ un codice ciclico di lunghezza $n$, indichiamo ancora con $C$ l'insieme delle
classi di equivalenza che rappresentano $C$ in $\mathbb{K}[x]/_{\bmod (1+x^n)}$,
chiamiamo generatore di $C$ il polinomio $g(x)$ (diverso da $0$) che ha grado minimo
tra tutti quelli contenuti nelle classi di equivalenza di $C$.
\end{defin}

Dato un codice ciclico $C$ e sia $\ve{v}\in C$. Allora dati $a(x)\in\mathbb{K}[x]$
(volendo si può scegliere $\m{deg}a(x)<$ lunghezza di $C$) e $v(x)$ il polinomio
associato alla parola $\ve{v}$ allora se $c(x)\in\mathbb{K}[x]$ è tale che
\begin{displaymath}
c(x)\equiv a(x)v(x)\bmod(1+x^n).
\end{displaymath}
ne segue che $c(x)$ \textit{rappresenta} una parola appartenente a $C$.

Il rappresentante canonico della classe di equivalenza
\begin{displaymath}
\begin{split}
\overline{f(x)}& =\{g(x)\in\mathbb{K}[x]:g(x)\equiv f(x)\bmod (1+x^n)\} \\
& = \{g(x)=f(x)+a(x)\cdot (1+x^n)\}
\end{split}
\end{displaymath}
è il polinomio $c(x)\in\overline{f(x)}$ di grado minimo.

\begin{ese}
Sia $f(x)=x+x^5+x^{12}$. Troviamo il rappresentante canonico della classe di equivalenza
\begin{displaymath}
\overline{f(x)}=\{g(x)\in\mathbb{K}[x]:g(x)\equiv f(x)\bmod (1+x^n)\}
\end{displaymath}
fissato $n=6$.

Avremo
\begin{displaymath}
\overline{f(x)}=\{g(x)\in\mathbb{K}[x]:g(x)\equiv f(x)\bmod (1+x^6)\}.
\end{displaymath}
\end{ese}
In tal caso il rappresentante canonico di $\overline{f(x)}$ sarà il resto della divisione
di $f(x)$ per $1+x^6$.
\begin{displaymath}
\polylongdiv[style=A]{x^{12}+x^{5}+x}{x^6+1}
\end{displaymath}
Quindi $x^5+x+1$ è il rappresentante canonico.

\begin{defin}
Sia $C$ un codice ciclico. Definiamo polinomio generatore di $C$, \textit{il} polinomio
\textit{non nullo} di grado minimo.
\end{defin}

\begin{oss}Sia $g(x)$ un polinomio che \textit{genera} un codice ciclico $C$. Se $\m{deg}g(x)=n-k$
i polinomi
\begin{equation}\label{poly}
\underbrace{g(x),xg(x),x^2g(x),\dots,x^{k-1}g(x)}_{k}
\end{equation}
costituiscono una base di $C$.

Inoltre $\m{dim}C=k$.
\end{oss}

I polinomi \eqref{poly} sono indipendenti perché sono tutti di grado diverso.

\begin{teo}
Il generatore di un codice ciclico (lineare) di lunghezza $n$ è necessariamente
un divisore del polinomio $1+x^n$.
\end{teo}

\begin{ese}
$1+x^2=(1+x)(1+x)$.
\end{ese}

\begin{defin}
Un polinomio si dice irriducibile se è divisibile soltanto per $1$ o per se stesso.
(Il polinomio $1$ non si condidera irriducibile).
\end{defin}

\begin{cor}
Dato il codice ciclico di $C$ (lunghezza di $C$ è $n$) e $v(x)\in C$, il generatore
di $C$ è il polinomio $g(x)=\m{MCD}(v(x),1+x^n)$.
\end{cor}

\begin{ese}
Calcoliamo $\m{MCD}(18,30)$ mediante il crivello di Eratostene. Scomponiamo $18$ e $30$
in fattori:
\begin{displaymath}
\begin{split}
18& = 3^2\cdot 2 \\
30& = 3\cdot 2\cdot 5.
\end{split}
\end{displaymath}
Il massimo comune divisore sarà dato dal prodotto dei fattori comuni con il minimo esponente quindi
\begin{displaymath}
\m{MCD}(18,30)=3\cdot 2=6.
\end{displaymath}
\end{ese}

\begin{ese}
Calcoliamo $\m{MCD}(18,30)$ mediante l'algoritmo di Euclide.
\begin{displaymath}
\begin{split}
30& =\underbrace{q_1}_{=1}\cdot 18+\underbrace{r_2}_{=12} \\
30& =\underbrace{q_1}_{=2}\cdot 12+\underbrace{r_2}_{=6}
\end{split}
\end{displaymath}
Quindi
\begin{displaymath}
\m{MCD}(18,30)=3\cdot 2=6.
\end{displaymath}
\end{ese}

%\begin{ese}
%Siano
%\begin{displaymath}
%v(x)=x^4+x^3+x+1,\hspace{3mm}x^8+1.
%\end{displaymath}
%$\m{MCD}(x^4+x^3+x+1,x^8+1)=$ \textit{il} generatore del codice ciclico che contiene $v(x)$.
%\begin{displaymath}
%\polylongdiv{x^8+1}{x^4+x^3+x+1}
%\end{displaymath}
%\end{ese}

\begin{ese}
Sia $C$ il codice ciclico generato da $g(x)=1+x+x^3,n=7$. Si avrà
\begin{displaymath}
\mathbb{K}^7\simeq\mathbb{K}[x]/(1+x^7)\simeq\mathbb{K}^6.
\end{displaymath}
Un insieme di generatori per $C$ è
\begin{displaymath}
g(x),xg(x),x^2g(x),x^3g(x),\dots
\end{displaymath}
quindi
\begin{displaymath}
\begin{split}
g(x)& = 1+x+x^3 \\
xg(x)& = x+x^2+x^4 \\
x^2g(x)& = x^2+x^3+x^5 \\
x^3g(x)& = x^3+x^4+x^6 \\
x^4g(x)& = x^4+x^5+x^7
\end{split}
\end{displaymath}
ma
\begin{displaymath}
x^4+x^5+x^7\equiv x^4+x^5+1(\bmod(1+x^7))
\end{displaymath}
pertanto
\begin{displaymath}
1+x^4+x^5\in\langle1+x+x^3,x+x^2+x^4,x^2+x^3+x^5,x^3+x^4+x^6 \rangle
\end{displaymath}
\end{ese}

\begin{ese}
Sia $\ve{v}=010101$. Il codice ciclico più piccolo contenente $\ve{v}$ sarà
\begin{displaymath}
C=\langle x+x^3+x^5,1+x^2+x^4 \rangle.
\end{displaymath}
Il polinomio generatore sarà $1+x^2+x^4$.
\end{ese}

\textbf{Codifica di un codice ciclico}. Siano
\begin{displaymath}
a(x)\in\mathbb{K}[x]_{\m{dim}C-1},\hspace{2mm}\m{deg}g(x)=n-k,\hspace{2mm}\m{deg}a(x)=n-1.
\end{displaymath}
Allora $a(x)\cdot g(x)$ è un polinomio di grado $n-1$. Questo completa la codifica.

\vspace{4mm}

\textbf{Decodifica di un codice ciclico}. Se $c(x)\in C$ allora $a(x)=c(x)\div g(x)$. Un
polinomio $c(x)$ appartiene al codice ciclico generato dal polinomio $g(x)$ se e solo se
il resto della divisione di $c(x)$ per $g(x)$ è $0$.

La matrice di parità $H=[\dots]$ sarà tale che $x_i=r_i(x)\bmod g(x), i=1,\dots,n-1$, 
cioè nelle righe avremo i coefficienti di $r_i(x)$.

Il polinomio $g(x)$ è un generatore di un codice ciclico di lunghezza $n$ se e solo se
$g(x)$ divide $1+x^n$. Il problema si riconduce, quindi, a costruire tutte le fattorizzazioni
di $1+x^n$.

\begin{ese}
$1+x^3=(1+x)(1+x+x^2)$.
\end{ese}

\begin{ese}
$1+x^4=(1+x^2)^2=(1+x)^4$.
\end{ese}

Il duale di un codice ciclico è ancora un codice ciclico.

Se $\pi$ è la permutazione ciclica $\ve{y}\in C^\bot\Rightarrow\pi(\ve{y})\in C^\bot$.
